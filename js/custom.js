//toggle icon in navigation
$(".navbar-toggler").click(function () {
    $(this).find("span").toggleClass("icon-bars").toggleClass("icon-close");
});


$(function () {
    var autosearch = [
        "New Collection",
        "Best Sellers",
        "Barbie Cakes",
        "Fruit Cakes",
        "Personalized Cakes",
        "Fancy Cakes",
        "Tier Cakes",
        "Sugar Free Cakes",
        "Tyheme Cakes",
        "Eesigner Cakes",
        "Best Collections",
        "Kids Cakes",
        "Chocolate Cakes",
        "Emoji Cakes",
        "Fresh Designs",
        "Flower Arrangements",
        "Flower Bouquet",
        "Basekt Arrangments",
        "Vase Arrangements",
        "Garlands",
        "Chocolate Bouquets",
        "Scheme"
    ];
    $("#search-items").autocomplete({
        source: autosearch
    });

    //auto cities

    var autocities = [
        "Andhra Pradesh",
        "Arunachal Pradesh",
        "Assam",
        "Bihar",
        "Chattisgarh",
        "Goa",
        "Gujarat",
        "Haryana",
        "Himachal Pradesh",
        "Jharkhand",
        "Karnataka",
        "Kerala",
        "Madha Pradesh",
        "Maharastra",
        "Manipur",
        "Meghalaya",
        "Mizoram",
        "Nagaland",
        "Odisha",
        "Punjab",
        "Rajasthan",
        "Sikkim",
        "Tamilnadu",
        "Telangana",
        "Tripura",
        "Uttarakhand",
        "Utter Pradesh",
        "West Bengal"

    ];
    $("#search-cities").autocomplete({
        source: autocities
    });
});


//click event to move top

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('#movetop').fadeIn();
    } else {
        $('#movetop').fadeOut();
    }
});

//Click event to scroll to top
$('#movetop').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
    return false;
});



//on scroll add class to header 

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.fixed-top').addClass('fixed-theme');
    } else {
        $('.fixed-top').removeClass('fixed-theme');
    }
});


$(function () {
    $(".datepicker").datepicker();
});

$('[data-toggle="popover"]').popover();

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


$(function () {
    $(".accordion").accordion({
        heightStyle: "content"
    });
});



$(document).ready(function () {
    //Horizontal Tab
    $('.parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
    // Child Tab
    $('.ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });
    //Vertical Tab
    $('.parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});




//hover menu dropdown
(function ($) {
    var defaults = {
        sm: 540,
        md: 720,
        lg: 960,
        xl: 1140,
        navbar_expand: 'lg'
    };
    $.fn.bootnavbar = function () {

        var screen_width = $(document).width();

        if (screen_width >= defaults.lg) {
            $(this).find('.dropdown').hover(function () {
                $(this).addClass('show');
                $(this).find('.dropdown-menu').first().addClass('show').addClass('animated fadeIn').one('animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd', function () {
                    $(this).removeClass('animated fadeIn');
                });
            }, function () {
                $(this).removeClass('show');
                $(this).find('.dropdown-menu').first().removeClass('show');
            });
        }

        $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            var $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

            return false;
        });
    };
})(jQuery);


//toggle icon in navigation
$(".navbar-toggler").click(function () {
    $(this).find("i").toggleClass("icon-bars").toggleClass("icon-close");
});


//remove cart items 
$(".closerow").click(function () {
    var hideCartItem = $(this).closest(".cartlist");
    hideCartItem.hide();
});

//cart page edit and time 
$("#save-dtandtime").click(function () {
    var hideCartSaveDate = $(this).closest("#changedt-time");
    hideCartSaveDate.slideUp();
});

//remove address from check out address
$(".removeaddress-checkout").click(function () {
    var hidecheckoutAddress = $(this).closest(".adresscolumn");
    hidecheckoutAddress.hide();
});

//user address delte 

$(".user-address-delte").click(function () {
    var hideUserAddress = $(this).closest(".user-addresscol")
    hideUserAddress.hide();
});


$(".user-intro").click(function () {
    $(".user-nav").slideToggle();
});